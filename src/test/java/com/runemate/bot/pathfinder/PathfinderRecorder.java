package com.runemate.bot.pathfinder;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.input.direct.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.api.script.framework.listeners.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.annotation.*;
import com.fasterxml.jackson.databind.module.*;
import java.io.*;
import java.util.*;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2
public class PathfinderRecorder extends LoopingBot implements MenuInteractionListener {

    private static final List<Integer> OBJECT_OPCODES = List.of(
        MenuOpcode.GAME_OBJECT_FIRST_OPTION.getId(),
        MenuOpcode.GAME_OBJECT_SECOND_OPTION.getId(),
        MenuOpcode.GAME_OBJECT_THIRD_OPTION.getId(),
        MenuOpcode.GAME_OBJECT_FOURTH_OPTION.getId(),
        MenuOpcode.GAME_OBJECT_FIFTH_OPTION.getId()
    );

    private ObjectMapper mapper;

    private int waitTicks;

    private int objectId;
    private String action;
    private Coordinate start;
    private Coordinate end;

    @Override
    public void onStart(final String... arguments) {
        setLoopDelay(600);
        getEventDispatcher().addListener(this);

//        SimpleModule module = new SimpleModule();
//        module.addSerializer(Empty.class, new EmptySerializer());

        mapper = new ObjectMapper()
//            .registerModule(module)
            .disable(SerializationFeature.FAIL_ON_SELF_REFERENCES)
            .enable(SerializationFeature.INDENT_OUTPUT);
    }

    @Override
    public void onLoop() {
        waitTicks--;
        if (waitTicks > 0) {
            return;
        }

        if (start != null) {
            Player local = Players.getLocal();
            if (local == null) {
                start = null;
                return;
            }

            if (local.isIdle() && !local.isMoving()) {
                if (Region.isInstanced()) {
                    this.end = Coordinate.uninstance(local.getPosition());
                } else {
                    this.end = local.getPosition();
                }
            }

            if (end != null) {
                if (end.equals(start)) {
                    end = null;
                    return;
                }

                Transport transport = new Transport(
                    new Position(start.getX(), start.getY(), start.getPlane()),
                    new Position(end.getX(), end.getY(), end.getPlane()),
                    action,
                    objectId
                );

                try {
                    String value = mapper.writeValueAsString(transport);

                    log.info("Transport: \n{}", value);
                } catch (JsonProcessingException e) {
                    log.warn("Unable to serialize transport", e);
                }

                start = null;
                end = null;
            }
        }
    }

    @Value
    private static class Transport {
        @JsonProperty("source") Position source;
        @JsonProperty("destination") Position destination;
        @JsonProperty("action") String action;
        @JsonProperty("objectId") int objectId;
        @JsonProperty("requirements") @JsonSerialize(using = EmptySerializer.class) Empty requirements = new Empty();
    }

    @Value
    private static class Position {
        int x;
        int y;
        int plane;
    }

    private static class Empty {}
    private static class EmptySerializer extends JsonSerializer<Empty> {

        @Override
        public void serialize(final Empty o, final JsonGenerator jg, final SerializerProvider serializerProvider) throws IOException {
            jg.writeStartObject();
            jg.writeEndObject();
        }
    }

    @Override
    public void onInteraction(MenuInteractionEvent event) {
        if (OBJECT_OPCODES.contains(event.getOpcode())) {
            GameObject target = (GameObject) event.getTargetEntity();
            if (target == null) {
                log.warn("Unable to resolve target GameObject for {}", event);
                return;
            }

            GameObjectDefinition definition = target.getActiveDefinition();
            if (definition == null) {
                log.warn("Unable to resolve definition of object {}", target);
                return;
            }

            GameObjectDefinition base = target.getDefinition();
            if (base != null && base.getId() != definition.getId()) {
                log.warn("BEWARE: Object has a transformation! varp={}, varbit={}", base.getStateVarp(), base.getStateVarbit());
            }

            Player local = Players.getLocal();
            if (local == null) {
                return;
            }

            int index = OBJECT_OPCODES.indexOf(event.getOpcode());
            String action = definition.getRawActions()[index];
            if (action == null) {
                log.warn("Unable to resolve action at index {} for object {}", index, target);
                return;
            }

            this.objectId = target.getId();
            this.action = action;
            this.waitTicks = 5;

            if (Region.isInstanced()) {
                this.start = Coordinate.uninstance(local.getPosition());
            } else {
                this.start = local.getPosition();
            }
        }
    }
}
