package com.runemate.game.api.hybrid.location.navigation;

import static com.runemate.game.api.hybrid.player_sense.PlayerSense.Key.*;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import javafx.scene.canvas.*;
import javax.annotation.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.apache.commons.lang3.*;

/**
 * A path through the game's landscape that is to be traversed no more than once without regeneration.
 */
@Log4j2
public abstract class Path implements Renderable, Validatable  {
    private static final TraversalOption[] EMPTY_TRAVERSAL_OPTIONS = new TraversalOption[0];
    public static final TraversalOption[] DEFAULT_TRAVERSAL_OPTIONS = {
        TraversalOption.MANAGE_RUN,
        TraversalOption.MANAGE_DISTANCE_BETWEEN_STEPS,
        TraversalOption.MANAGE_STAMINA_ENHANCERS
    };

    /**
     * @return Takes a step along the path (manage = true)
     */
    public final boolean step() {
        return step(true);
    }

    /**
     * Takes a step along the path using either all or none of the TraversalOptions
     *
     * @param manage whether or not to manage the distance between steps to prevent spam clicking
     * @return true if it took a step, otherwise false
     */
    public final boolean step(boolean manage) {
        return step(manage ? DEFAULT_TRAVERSAL_OPTIONS : EMPTY_TRAVERSAL_OPTIONS);
    }

    public abstract boolean step(@NonNull TraversalOption... options);

    /**
     * Gets a list of the vertices in this path
     *
     * @return A List
     */
    public abstract List<? extends Locatable> getVertices();

    /**
     * @return the next Locatable within the path.
     */
    @Nullable
    public abstract Locatable getNext();

    public abstract Locatable getNext(boolean preferViewportTraversal);

    @Override
    public void render(Graphics2D g2d) {
        for (final Locatable l : getVertices()) {
            final Area a = l.getArea();
            if (a != null) {
                a.render(g2d);
            }
        }
    }

    public void render(GraphicsContext gc) {
        for (final Locatable l : getVertices()) {
            if (l instanceof Renderable) {
                ((Renderable) l).render(gc);
            } else {
                final Coordinate c = l.getPosition();
                if (c != null) {
                    final Point p = c.minimap().getInteractionPoint();
                    if (p != null) {
                        gc.fillRect(p.x - 1, p.y - 1, 2, 2);
                    }
                }
            }
        }
    }

    protected boolean isEligibleToStep(@NonNull TraversalOption... options) {
        if (!ArrayUtils.contains(options, TraversalOption.MANAGE_DISTANCE_BETWEEN_STEPS)) {
            return true;
        }
        var destination = Traversal.getDestination();
        if (destination == null) {
            return true;
        }

        final var local = Optional.ofNullable(Players.getLocal())
            .map(Actor::getServerPosition)
            .orElse(null);

        if (local == null) {
            return false;
        }

        final var last = getLast();
        if (last != null && local.equals(last.getPosition())) {
            return false;
        }

        if (getVertices().isEmpty()) {
            return false;
        }

        //When running we can expand this threshold but cap it at a reasonable number
        var threshold = PlayerSense.getAsDouble(STEP_THRESHOLD);
        if (Traversal.isRunEnabled()) {
            threshold = Math.min(14, threshold * 1.5);
        }

        log.trace("Using step distance threshold of {}", threshold);
        return Distance.between(local, destination, Distance.Algorithm.CHEBYSHEV) <= threshold //Close enough to destination
            || Distance.between(local, destination, Distance.Algorithm.CHEBYSHEV) > 14; //Too far from destination (if we miss-clicked etc.)
    }

    protected boolean triggerRun(@NonNull TraversalOption... options) {
        if (!ArrayUtils.contains(options, TraversalOption.MANAGE_RUN)
            || Traversal.isRunEnabled()
            || Traversal.getRunEnergy() < PlayerSense.getAsInteger(ENABLE_RUN_AT)) {
            return true;
        }

        return Traversal.toggleRun();
    }

    protected boolean triggerStaminaEnhancement(@NonNull TraversalOption... options) {
        if (!ArrayUtils.contains(options, TraversalOption.MANAGE_STAMINA_ENHANCERS)) {
            return true;
        }

        if (Traversal.isStaminaEnhanced()
            || !Traversal.hasStaminaEnhancer()
            || Traversal.getRunEnergy() > PlayerSense.getAsInteger(ENABLE_STAMINA_ENHANCERS_BELOW)) {
            return true;
        }

        if (Inventory.isItemSelected() || Bank.isOpen()) {
            return true;
        }

        return Traversal.drinkStaminaEnhancer(false);
    }

    public int getLength() {
        int length = 0;
        Locatable previous = null;
        for (Locatable vertex : getVertices()) {
            if (previous != null) {
                var pp = previous.getPosition();
                var cp = vertex.getPosition();

                //If planes are not equal, distance will be Double.POSITIVE_INFINITY
                if (pp != null && cp != null && pp.getPlane() == cp.getPlane()) {
                    length += Distance.between(previous, vertex);
                } else {
                    length++;
                }
            }
            previous = vertex;
        }
        return length;
    }

    public Locatable getLast() {
        var vertices = getVertices();
        return vertices.isEmpty() ? null : vertices.get(vertices.size() - 1);
    }

    @Override
    public boolean isValid() {
        Coordinate local = Optional.ofNullable(Players.getLocal()).map(Actor::getServerPosition).orElse(null);
        Set<Coordinate> reachable;
        return local != null
            && (reachable = Optional.ofNullable(local.getPosition()).map(Coordinate::getReachableCoordinates).orElse(null)) != null
            && getVertices().stream().anyMatch(x -> Distance.between(local, x) < 5 && reachable.contains(x.getPosition()));
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder("Path[");
        Iterator<? extends Locatable> iterator = getVertices().iterator();
        while (iterator.hasNext()) {
            string.append(iterator.next());
            if (iterator.hasNext()) {
                string.append(" -> ");
            }
        }
        string.append(']');
        return string.toString();
    }

    /**
     * Options that control the way the path is traversed
     */
    public enum TraversalOption {
        PREFER_VIEWPORT,
        MANAGE_RUN,
        MANAGE_DISTANCE_BETWEEN_STEPS,
        MANAGE_STAMINA_ENHANCERS,
        USE_DIRECT_INPUT
    }
}
