package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.teleports;

import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.util.collections.*;
import java.io.*;
import java.util.*;
import lombok.*;
import org.jetbrains.annotations.*;

public abstract class TeleportVertex extends WebVertex {
    public TeleportVertex(
        final Coordinate destination,
        final Collection<WebRequirement> requirements
    ) {
        super(destination, requirements);
    }

    public TeleportVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    @NonNull
    @Override
    public Pair<WebVertex, WebPath.VertexSearchAction> getStep(Map<String, Object> args) {
        return new Pair<>(this, WebPath.VertexSearchAction.CONTINUE);
    }

    @Override
    public boolean step(boolean prefersViewport) {
        return step();
    }

    @Override
    public String toString() {
        Coordinate position = getPosition();
        return "TeleportVertex(" + position.getX() + ", " + position.getY() + ", " +
            position.getPlane() + ')';
    }
}
