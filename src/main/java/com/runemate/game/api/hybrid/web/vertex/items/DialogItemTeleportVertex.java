package com.runemate.game.api.hybrid.web.vertex.items;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.input.direct.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.web.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.framework.core.*;
import java.util.*;
import java.util.regex.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2
public class DialogItemTeleportVertex extends BasicItemTeleportVertex {
    
    private final Pattern dialog;

    public DialogItemTeleportVertex(final Coordinate position, final Pattern action, final Pattern dialog, final SpriteItemQueryBuilder builder) {
        super(position, action, builder);
        this.dialog = dialog;
    }

    @Override
    public boolean step(final Map<String, Object> cache) {
        final var local = (Player) cache.get(WebPath.AVATAR);
        final var localPos = (Coordinate) cache.get(WebPath.AVATAR_POS);
        if (local == null || localPos == null) {
            return false;
        }

        if (isDialogAvailable()) {
            return handleDialog();
        }

        final var item = getItem();
        if (item == null) {
            log.warn("Failed to resolve target entity for {}", this);
            return false;
        }

        if ((boolean) cache.get(WebPath.DIRECT_INPUT)) {
            var ma = MenuAction.forSpriteItem(item, action);
            if (ma != null) {
                DirectInput.send(ma);
                return Execution.delayUntil(this::isDialogAvailable, () -> local.getAnimationId() != -1, 3000);
            }
        }

        return item.interact(action) && Execution.delayUntil(this::isDialogAvailable, 3000);
    }

    private boolean isDialogAvailable() {
        return ChatDialog.getContinue() != null
            || ChatDialog.getOption(dialog) != null
            || getHeadsUpComponent() != null;
    }

    private boolean handleDialog() {
        ChatDialog.Selectable selectable;
        if ((selectable = ChatDialog.getContinue()) != null) {
            return selectable.select();
        } else if ((selectable = ChatDialog.getOption(dialog)) != null) {
            return selectable.select();
        }

        var iface = getHeadsUpComponent();
        return iface != null && iface.click();
    }

    private @Nullable InterfaceComponent getHeadsUpComponent() {
        return Interfaces.newQuery().containers(187).types(InterfaceComponent.Type.LABEL).texts(dialog).results().first();
    }

    @Override
    public String toString() {
        final var joiner = new StringJoiner(",", "DialogItemTeleportVertex(", ")");
        joiner.add("position=" + position);
        joiner.add("action=" + action.pattern());
        joiner.add("option=" + dialog.pattern());
        if (BotPlatform.isBotThread()) {
            var result = getItem();
            joiner.add("item=" + result);
        }
        return joiner.toString();
    }
}
