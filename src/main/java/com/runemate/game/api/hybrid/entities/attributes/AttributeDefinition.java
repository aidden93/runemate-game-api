package com.runemate.game.api.hybrid.entities.attributes;

import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.cache.*;
import java.io.*;
import org.jetbrains.annotations.*;
import lombok.extern.log4j.*;

@Log4j2
public abstract class AttributeDefinition {

    private static final ParameterDefinitionLoader osrsDefLoader = new ParameterDefinitionLoader(-1, true);

    @Nullable
    public static AttributeDefinition load(int id) {
        if (id >= 0) {
            try {
                CacheParameterDefinition bdef = osrsDefLoader.load(JS5CacheController.getLargestJS5CacheController(), -1, id);
                if (bdef != null) {
                    return bdef.extended();
                }
            } catch (final IOException ioe) {
                log.warn("Unable to load attribute definition for {}", id, ioe);
            }
        }
        return null;
    }

    public abstract int getDefaultInt();

    @Nullable
    public abstract String getDefaultString();
}
