package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.teleports;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.net.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.*;
import java.io.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import lombok.*;
import org.apache.commons.lang3.builder.*;

public class BasicItemTeleportVertex extends ItemTeleportVertex implements SerializableVertex {
    private SpriteItem.Origin origin;
    private Pattern name, action;
    private int id = -1;

    public BasicItemTeleportVertex(
        final Coordinate destination,
        final SpriteItem.Origin origin, final int id,
        final Pattern action,
        final Collection<WebRequirement> conditions
    ) {
        super(destination, conditions);
        this.origin = origin;
        this.id = id;
        this.action = action;
    }

    public BasicItemTeleportVertex(
        final Coordinate destination,
        final SpriteItem.Origin origin, final Pattern name,
        final Pattern action,
        final Collection<WebRequirement> conditions
    ) {
        super(destination, conditions);
        this.origin = origin;
        this.name = name;
        this.action = action;
    }

    public BasicItemTeleportVertex(
        final Coordinate destination,
        final SpriteItem.Origin origin, final String name,
        final String action,
        final Collection<WebRequirement> conditions
    ) {
        this(destination, origin, new String[] { name }, action, conditions);
    }

    public BasicItemTeleportVertex(
        final Coordinate destination,
        final SpriteItem.Origin origin, final String[] names,
        final String action,
        final Collection<WebRequirement> conditions
    ) {
        this(destination, origin, Regex.getPatternForExactStrings(names),
            Regex.getPatternForExactString(action), conditions
        );
    }

    public BasicItemTeleportVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    public int getId() {
        return id;
    }

    public SpriteItem.Origin getOrigin() {
        return origin;
    }

    public Pattern getNamePattern() {
        return name;
    }

    public Pattern getActionPattern() {
        return action;
    }

    @Override
    public SpriteItem getItem() {
        SpriteItemQueryBuilder builder = null;
        if (origin == SpriteItem.Origin.INVENTORY) {
            builder = Inventory.newQuery();
        } else if (origin == SpriteItem.Origin.EQUIPMENT) {
            builder = Equipment.newQuery();
        }
        if (builder == null) {
            throw new UnsupportedOperationException("SpriteItems with an origin of " + origin +
                " are not currently supported by this vertex");
        }
        if (id != -1) {
            builder.ids(id);
        }
        if (name != null) {
            builder.names(name);
        }
        if (action != null) {
            builder.actions(action);
        }
        if (provider != null) {
            builder.provider(() -> provider);
        }
        return builder.results().first();
    }

    @Override
    public Predicate<SpriteItem> getFilter() {
        SpriteItemQueryBuilder builder = null;
        if (origin == SpriteItem.Origin.INVENTORY) {
            builder = Inventory.newQuery();
        } else if (origin == SpriteItem.Origin.EQUIPMENT) {
            builder = Equipment.newQuery();
        }
        if (builder == null) {
            throw new UnsupportedOperationException("SpriteItems with an origin of " + origin +
                " are not currently supported by this vertex");
        }
        if (id != -1) {
            builder.ids(id);
        }
        if (name != null) {
            builder.names(name);
        }
        if (action != null) {
            builder.actions(action);
        }
        return builder::accepts;
    }

    @Override
    public void invalidateCache() {
        provider = null;
    }

    @Override
    public boolean step() {
        final SpriteItem item = getItem();
        if (item == null) {
            return false;
        }
        int originalQuantity = item.getQuantity();
        Player player = Players.getLocal();
        if (player == null) {
            return false;
        }
        Coordinate position = player.getPosition();
        return (!Bank.isOpen() || Bank.close())
            && (!GrandExchange.isOpen() || GrandExchange.close())
            && item.interact(action)
            && Execution.delayUntil(() -> !position.equals(player.getPosition()) &&
                (!item.isValid() || item.getQuantity() != originalQuantity),
            () -> player.getAnimationId() != -1, 1800, 3000
        );
    }

    @Override
    public int hashCode() {
        if (cachedHashcode == -1) {
            HashCodeBuilder builder =
                new HashCodeBuilder().append(getPosition()).append(getOrigin().name());
            if (name != null) {
                builder.append(name.pattern()).append(name.flags());
            } else if (id != -1) {
                builder.append(id);
            }
            cachedHashcode = builder.append(action.pattern()).append(action.flags()).toHashCode();
        }
        return cachedHashcode;
    }

    @Override
    public String toString() {
        Coordinate pos = getPosition();
        return "BasicItemTeleportVertex(origin=" + getOrigin() + ", " + (name != null ? name : id)
            + ", action=" + action + ", x=" + pos.getX() + ", y=" + pos.getY() + ", plane=" +
            pos.getPlane() + ")";
    }

    @Override
    public int getOpcode() {
        return 3;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeUTF(origin.name());
        stream.writeInt(id);
        if (id == -1) {
            stream.writeUTF(name.pattern());
            stream.writeInt(name.flags());
        }
        stream.writeUTF(action.pattern());
        stream.writeInt(action.flags());
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        origin = SpriteItem.Origin.valueOf(stream.readUTF());
        id = stream.readInt();
        if (id == -1) {
            name = Pattern.compile(stream.readUTF(), stream.readInt());
        }
        action = Pattern.compile(stream.readUTF(), stream.readInt());
        return true;
    }
}
