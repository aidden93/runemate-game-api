package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.objects;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.collections.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2
public abstract class ObjectVertex extends WebVertex {

    protected Pattern target;
    protected Pattern action;
    protected GameObject.Type type;

    protected List<GameObject> provider;

    public ObjectVertex(
        final int x, final int y, final int plane, final Pattern target, final Pattern action, final Collection<WebRequirement> requirements
    ) {
        this(new Coordinate(x, y, plane), target, action, requirements);
    }

    public ObjectVertex(
        final Coordinate position, final Pattern target, final Pattern action, final Collection<WebRequirement> requirements
    ) {
        super(position, requirements);
        this.target = target;
        this.action = action;
    }

    public ObjectVertex(
        final Coordinate position,
        final Pattern target,
        final Pattern action,
        final Collection<WebRequirement> requirements,
        final Collection<WebRequirement> blockingConditions
    ) {
        super(position, requirements, blockingConditions);
        this.target = target;
        this.action = action;
    }

    public ObjectVertex(
        final int x, final int y, final int plane, final Pattern target, final String action, final Collection<WebRequirement> requirements
    ) {
        this(new Coordinate(x, y, plane), target, Regex.getPatternForExactString(action), requirements);
    }

    public ObjectVertex(
        final Coordinate position, final Pattern target, final String action, final Collection<WebRequirement> requirements
    ) {
        this(position, target, Regex.getPatternForExactString(action), requirements);
    }

    public ObjectVertex(
        final int x, final int y, final int plane, final String target, final Pattern action, final Collection<WebRequirement> requirements
    ) {
        this(new Coordinate(x, y, plane), Regex.getPatternForExactString(target), action, requirements);
    }

    public ObjectVertex(
        final Coordinate position, final String target, final Pattern action, final Collection<WebRequirement> requirements
    ) {
        this(position, Regex.getPatternForExactString(target), action, requirements);
    }

    public ObjectVertex(
        final int x, final int y, final int plane, final String target, final String action, final Collection<WebRequirement> requirements
    ) {
        this(new Coordinate(x, y, plane), Regex.getPatternForExactString(target), Regex.getPatternForExactString(action), requirements);
    }

    public ObjectVertex(
        final int x,
        final int y,
        final int plane,
        final String target,
        final String action,
        final Collection<WebRequirement> requirements,
        Collection<WebRequirement> blockingConditions
    ) {
        this(new Coordinate(x, y, plane),
            Regex.getPatternForExactString(target),
            Regex.getPatternForExactString(action),
            requirements,
            blockingConditions
        );
    }

    public ObjectVertex(
        final Coordinate position, final String target, final String action, final Collection<WebRequirement> requirements
    ) {
        this(position, Regex.getPatternForExactString(target), Regex.getPatternForExactString(action), requirements);
    }

    public ObjectVertex(
        final Coordinate position,
        final String target,
        final String action,
        final Collection<WebRequirement> requirements,
        Collection<WebRequirement> blockingConditions
    ) {
        this(position, Regex.getPatternForExactString(target), Regex.getPatternForExactString(action), requirements, blockingConditions);
    }

    public ObjectVertex(
        Coordinate position,
        Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements,
        int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    public GameObject getObject() {
        final GameObjectQueryBuilder builder = GameObjects.newQuery().on(getPosition());
        if (target != null) {
            builder.names(target);
        }
        if (action != null) {
            builder.actions(action);
        }
        if (type != null) {
            builder.types(type);
        }
        if (provider != null) {
            builder.provider(() -> provider);
        }
        LocatableEntityQueryResults<GameObject> results = builder.results();
        if (results.size() > 1) {
            log.warn("{} results when querying {}", results.size(), this);
        }
        GameObject object = results.first();
        if (type == null && object != null) {
            type = object.getType();
        }
        return object;
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public Pair<WebVertex, WebPath.VertexSearchAction> getStep(Map<String, Object> args) {
        final WebVertex previousVertex = (WebVertex) args.get(PREVIOUS);
        final Coordinate vertexPosition = getPosition();
        final Area.Rectangular region = (Area.Rectangular) args.get(SCENE);
        final Set<Coordinate> reachable = (Set<Coordinate>) args.get(REACHABLE);
        if (region.contains(vertexPosition)) {
            final GameObject object = getObject();
            if (object != null) {
                Coordinate stepBeforeObject = null;
                if (previousVertex != null) {
                    Coordinate previousCoordinate = previousVertex.getPosition();
                    if (reachable.contains(previousCoordinate)) {
                        stepBeforeObject = previousCoordinate;
                    }
                }
                if (stepBeforeObject == null) {
                    for (Coordinate surrounding : object.getArea().getSurroundingCoordinates()) {
                        if (reachable.contains(surrounding)) {
                            stepBeforeObject = surrounding;
                            break;
                        }
                    }
                }
                if (stepBeforeObject == null || reachable.contains(stepBeforeObject)) {
                    if (object.isVisible()) {//Door in monestary underground isn't visible when on the tile.
                        return new Pair<>(this, WebPath.VertexSearchAction.STOP);
                    } else if (stepBeforeObject != null && stepBeforeObject.minimap().isVisible(region, args)) {
                        return new Pair<>(new WebPath.PseudoCoordinateVertex(vertexPosition, Collections.emptyList()),
                            WebPath.VertexSearchAction.STOP
                        );
                    }
                }
            }
        }
        return new Pair<>(null, WebPath.VertexSearchAction.CONTINUE);
    }

    @Override
    public final boolean step(boolean prefersViewport) {
        return step();
    }


    public Pattern getActionPattern() {
        return action;
    }

    public Pattern getTargetPattern() {
        return target;
    }

    @Override
    public String toString() {
        Coordinate position = getPosition();
        return "ObjectVertex(" + position.getX() + ", " + position.getY() + ", " + position.getPlane() + ')';
    }
}
