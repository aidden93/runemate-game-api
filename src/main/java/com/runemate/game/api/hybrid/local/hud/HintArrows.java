package com.runemate.game.api.hybrid.local.hud;

import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.local.hud.*;
import java.util.function.*;

public final class HintArrows {
    private HintArrows() {
    }

    public static HintArrowQueryBuilder newQuery() {
        return new HintArrowQueryBuilder();
    }

    public static HintArrowQueryResults getLoaded(final Predicate<HintArrow> filter) {
        return OSRSHintArrows.getLoaded(filter);
    }

    public static HintArrowQueryResults getLoaded() {
        return getLoaded(null);
    }
}
