package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class DBTableLoader extends SerializedFileLoader<DBTable> {

    public DBTableLoader() {
        super(CacheIndex.CONFIGS.getId());
    }

    @Override
    protected DBTable construct(final int entry, final int file, final Map<String, Object> arguments) {
        return new DBTable(file);
    }
}
