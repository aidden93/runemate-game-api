package com.runemate.game.api.hybrid.entities.details;

@FunctionalInterface
public interface Identifiable {
    int getId();
}
