package com.runemate.game.api.hybrid;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.google.common.cache.*;
import java.lang.reflect.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import org.jetbrains.annotations.*;

public class EquipmentLoadout extends EnumMap<Equipment.Slot, Pattern> {

    private static final Cache<String, ItemDefinition> CACHE = CacheBuilder.newBuilder().expireAfterAccess(5, TimeUnit.MINUTES).build();

    public EquipmentLoadout() {
        super(Equipment.Slot.class);
    }

    @Override
    public int size() {
        var size = 0;
        for (var entry : entrySet()) {
            if (entry.getValue() != null) {
                size++;
            }
        }
        return size;
    }

    public @Nullable ItemDefinition getDefinition(Equipment.Slot key, List<ItemDefinition> candidates) {
        final Pattern pattern = get(key);
        if (pattern == null) {
            return null;
        }
        ItemDefinition definition = CACHE.getIfPresent(pattern.pattern());
        if (definition == null) {
            definition = candidates.isEmpty() ? null : candidates.stream()
                .filter(candidate -> pattern.matcher(candidate.getName()).matches())
                .findFirst()
                .orElse(null);
            if (definition != null) {
                CACHE.put(pattern.pattern(), definition);
            }
        }
        return definition;
    }

    public @Nullable ItemDefinition getDefinition(Equipment.Slot key) {
        final Pattern pattern = get(key);
        if (pattern == null) {
            return null;
        }
        ItemDefinition definition = CACHE.getIfPresent(pattern.pattern());
        return definition == null ? getDefinition(key, ItemDefinition.loadAll(loadout())) : definition;
    }

    /**
     * @return the slots that do not contain items that are part of the EquipmentLoadout
     */
    public EnumSet<Equipment.Slot> getMissingSlots() {
        var result = EnumSet.noneOf(Equipment.Slot.class);
        for (var entry : entrySet()) {
            final var equipped = Equipment.getItemIn(entry.getKey());
            if (equipped == null || !named(entry.getValue()).test(equipped.getDefinition())) {
                result.add(entry.getKey());
            }
        }
        return result;
    }

    private Predicate<ItemDefinition> named(Pattern pattern) {
        return definition -> {
            if (definition == null) {
                return false;
            }
            final var item = definition.getName();
            return pattern.matcher(item).matches();
        };
    }

    public Predicate<ItemDefinition> loadout() {
        return definition -> {
            if (definition == null) {
                return false;
            }
            final var item = definition.getName();
            return values().stream().filter(Objects::nonNull).anyMatch(pattern -> pattern.matcher(item).matches());
        };
    }

    public static class SettingConverter implements com.runemate.ui.setting.open.SettingConverter {

        @Override
        public Object fromString(final String s, final Type type) {
            if (s == null || s.isEmpty()) {
                return new EquipmentLoadout();
            }
            if (type == EquipmentLoadout.class) {
                final var split = s.split(";;");
                final var loadout = new EquipmentLoadout();
                for (var entry : split) {
                    var values = entry.split("##");
                    if (values.length != 2) {
                        throw new IllegalStateException("failed to parse entry from " + entry);
                    }

                    var slot = Equipment.Slot.valueOf(values[0]);
                    var pattern = Pattern.compile(values[1]);
                    loadout.put(slot, pattern);
                }
                return loadout;
            }
            return null;
        }

        @Override
        public String toString(final Object object) {
            if (object instanceof EquipmentLoadout) {
                var loadout = (EquipmentLoadout) object;
                var builder = new StringJoiner(";;");
                for (var entry : loadout.entrySet()) {
                    if (entry.getValue() == null) {
                        continue;
                    }
                    var value = entry.getKey().name() + "##" + entry.getValue().pattern();
                    builder.add(value);
                }
                return builder.toString();
            }
            return null;
        }
    }
}
