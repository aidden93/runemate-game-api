package com.runemate.game.api.script.framework.listeners.dispatchers;

import com.runemate.game.api.hybrid.net.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;
import java.util.stream.*;

public class GrandExchangeDispatcher extends IngameEventDispatcher {

    private List<Wrapper> last;

    @Override
    public int getIterationRateInMilliseconds() {
        return 3000;
    }

    @Override
    public void dispatch(EventDispatcher dispatcher) {
        if (last == null) {
            last = GrandExchange.getSlots().stream().map(Wrapper::new).collect(Collectors.toList());
        } else {
            final List<GrandExchange.Slot> current = GrandExchange.getSlots();
            if (last.size() != current.size()) {
                clear();
                return;
            }

            for (final GrandExchange.Slot slot : current) {
                for (final Wrapper old : last) {
                    if (old.index == slot.getIndex()) {
                        final GrandExchange.Offer offer = slot.getOffer();
                        double completion = -1;
                        GrandExchange.Offer.State state = null;
                        if (offer != null) {
                            state = offer.getState();
                            completion = offer.getCompletion();
                        }
                        if (old.offerState != state) {
                            if (old.offerState == null) {
                                dispatcher.dispatchLater(new GrandExchangeEvent(
                                    slot,
                                    GrandExchangeEvent.Type.OFFER_CREATED
                                ));
                            } else if (state == null) {
                                dispatcher.dispatchLater(new GrandExchangeEvent(
                                    slot,
                                    GrandExchangeEvent.Type.OFFER_REMOVED
                                ));
                            } else if (state != GrandExchange.Offer.State.COMPLETING
                                && state != GrandExchange.Offer.State.CANCELLING) {
                                dispatcher.dispatchLater(new GrandExchangeEvent(
                                    slot,
                                    GrandExchangeEvent.Type.OFFER_STATE_CHANGED
                                ));
                            }
                        } else if (old.offerCompletion != -1D && completion != -1D
                            && Double.compare(old.offerCompletion, completion) != 0) {
                            dispatcher.dispatchLater(new GrandExchangeEvent(
                                slot,
                                GrandExchangeEvent.Type.OFFER_COMPLETION_CHANGED
                            ));
                        }

                        old.offerState = state;
                        old.offerCompletion = completion;
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void clear() {
        last = null;
    }

    private static class Wrapper {

        private final int index;
        private GrandExchange.Offer.State offerState;
        private double offerCompletion;

        Wrapper(GrandExchange.Slot slot) {
            this.index = slot.getIndex();

            final GrandExchange.Offer offer = slot.getOffer();
            if (offer != null) {
                offerState = offer.getState();
                offerCompletion = offer.getCompletion();
            } else {
                offerState = null;
                offerCompletion = -1;
            }
        }
    }
}
