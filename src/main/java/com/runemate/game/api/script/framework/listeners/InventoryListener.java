package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

public interface InventoryListener extends EventListener {

    /**
     * @deprecated replaced by {@link #onInventoryUpdated(ItemEvent)}
     */
    default void onItemAdded(final ItemEvent event) {
    }

    /**
     * @deprecated replaced by {@link #onInventoryUpdated(ItemEvent)}
     */
    @Deprecated
    default void onItemRemoved(final ItemEvent event) {
    }

    default void onInventoryUpdated(ItemEvent event) {
    }
}
