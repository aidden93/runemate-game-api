package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.EventListener;

public interface NpcListener extends EventListener {

    default void onNpcHitsplat(HitsplatEvent event) {
    }

    default void onNpcSpawned(NpcSpawnedEvent event) {
    }

    //TODO change to CombatInfo change
    default void onNpcDeath(DeathEvent event) {
    }

    default void onNpcAnimationChanged(AnimationEvent event) {
    }

    default void onNpcTargetChanged(TargetEvent event) {
    }

    default void onNpcDespawned(NpcDespawnedEvent event) {
    }

    default void onNpcDefinitionChanged(NpcDefinitionChangedEvent event) {
    }

}
