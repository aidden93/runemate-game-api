package com.runemate.game.api.script.framework.tree;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.script.framework.logger.*;
import lombok.extern.log4j.*;

@Log4j2
public abstract class TreeTask {
    /**
     * This constructor is default access so that it cannot be
     * extended directly outside of the framework package.
     */
    TreeTask() {
    }

    public abstract boolean validate();

    public abstract void execute();

    /**
     * @return TreeTask - The task that should be evaluated on success of validation.
     */
    public abstract TreeTask successTask();

    /**
     * @return TreeTask - The task that should be evaluated on failure of validation.
     */
    public abstract TreeTask failureTask();

    /**
     * @return boolean - Whether or not this node is a leaf.
     */
    public abstract boolean isLeaf();

    /**
     * @return the logger instance associated with this bot instance.
     */
    @Deprecated
    public final BotLogger getLogger() {
        return Environment.getLogger();
    }
}
