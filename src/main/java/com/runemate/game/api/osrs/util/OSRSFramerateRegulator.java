package com.runemate.game.api.osrs.util;

import com.runemate.client.game.open.*;
import com.runemate.game.api.script.*;
import com.runemate.rmi.*;
import java.rmi.*;
import lombok.extern.log4j.*;

//Based loosely on RuneLites FPS capping implementation.
@Log4j2
public class OSRSFramerateRegulator implements Runnable {
    private static final int SAMPLE_SIZE = 3;
    private int targetMaxFPS;

    private long targetDelay = 0;
    private long lastMillis = 0;
    private long[] lastDelays;
    private int lastDelayIndex = 0;
    private long sleepDelay = 0;

    private int getConfiguredMaxFPS() {
        return OpenClientPreferences.getMaxFpsSetting();
    }

    /**
     * Done once at initialization and then done again anytime the focus changes or one of
     * the fps caps changes.
     */
    private void initialize(int maxFPS) {
        lastMillis = System.currentTimeMillis();
        targetDelay = 1000 / maxFPS;
        sleepDelay = targetDelay;
        lastDelays = new long[SAMPLE_SIZE];
        for (int i = 0; i < SAMPLE_SIZE; i++) {
            lastDelays[i] = targetDelay;
        }
        targetMaxFPS = maxFPS;

        try {
            OpenClient.registerEvents(Callback.RENDER);
        } catch (Exception e) {
            log.warn("Failed to initialize framerate regulator", e);
        }
    }

    @Override
    public void run() {
        int configuredMaxFPS = getConfiguredMaxFPS();
        if (targetMaxFPS != configuredMaxFPS) {
            initialize(configuredMaxFPS);
            return;
        }
        if (configuredMaxFPS == 50) {
            return;
        }
        // We can't trust client.getFPS to get frame-perfect FPS knowledge
        // If we do try to use client.getFPS, we will end up oscillating
        // So we rely on currentTimeMillis which is occasionally cached by the JVM unlike nanotime
        // Its caching will not cause oscillation as it is granular enough for our uses here
        final long before = lastMillis;
        final long now = System.currentTimeMillis();
        lastMillis = now;
        lastDelayIndex = (lastDelayIndex + 1) % SAMPLE_SIZE;
        lastDelays[lastDelayIndex] = now - before;

        // We take a sampling approach because sometimes the game client seems to repaint
        // after only running 1 game cycle, and then runs repaint again after running 30 cycles
        long averageDelay = 0;
        for (int i = 0; i < SAMPLE_SIZE; i++) {
            averageDelay += lastDelays[i];
        }
        averageDelay /= lastDelays.length;
        // A cycle doesn't necessarily run once every exact 20ms.
        // There can be some variation in either direction depending on the current scenes intensity.
        if (averageDelay > targetDelay) {
            sleepDelay--;
        } else if (averageDelay < targetDelay) {
            sleepDelay++;
        }

        if (sleepDelay > 0) {
            Execution.delay(sleepDelay);
        }
    }
}
