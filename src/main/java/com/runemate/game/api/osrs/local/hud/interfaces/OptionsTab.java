package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.*;
import java.awt.event.*;
import java.util.*;
import java.util.regex.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2
public class OptionsTab {

    private final static Pattern SEARCH_PATTERN = Pattern.compile(".*\\*");

    /**
     * Also known as the type of game client layout.
     *
     * @return The current CanvasMode
     */
    public static CanvasMode getCanvasMode() {
        return OSRSInterfaceOptions.isViewportResizable() ? CanvasMode.RESIZABLE : CanvasMode.FIXED;
    }

    /**
     * @return Whether the CanvasMode was set.
     */
    public static boolean setCanvasMode(CanvasMode mode) {
        if (mode.equals(getCanvasMode())) {
            return true;
        } else if (Menu.DISPLAY.open()) {
            log.info("Setting canvas mode to {}", mode);
            InterfaceComponent dropdownComponent = Interfaces.newQuery()
                .containers(116)
                .types(InterfaceComponent.Type.LABEL)
                .visible()
                .widths(142)
                .results()
                .first();
            if (dropdownComponent != null && dropdownComponent.click()) {
                Execution.delay(100, 250);
            }
            InterfaceComponent dropdownBox;
            InterfaceComponentQueryResults dropdownEntries = Interfaces.newQuery()
                .containers(116)
                .grandchildren(true)
                .actions("Select")
                .visible()
                .results();
            if (dropdownEntries.size() == 3) {
                if (CanvasMode.FIXED.equals(mode)) {
                    dropdownBox = dropdownEntries.get(0);
                } else {
                    /*
                        There are now two different resizable game client layouts, the modern which has the tabs in a line
                        and the classic which has the tabs laid out similarly to fixed - classic
                     */
                    dropdownBox = dropdownEntries.get(PlayerSense.getAsBoolean(PlayerSense.Key.PREFER_CLASSIC_RESIZABLE_HUD) ? 1 : 2);
                }
            } else {
                return false;
            }
            if (dropdownBox != null && dropdownBox.interact("Select")) {
                return Execution.delayUntil(() -> mode.equals(getCanvasMode()), 1800, 2400);
            }
        }
        return mode.equals(getCanvasMode());
    }

    public static boolean isAcceptingAid() {
        Varbit varbit = Varbits.load(4180);
        return varbit != null && varbit.getValue() == 1;
    }

    public static boolean setAcceptingAid(boolean accept) {
        if (isAcceptingAid() == accept) {
            return true;
        }
        log.info("Toggling accept aid {}", accept ? "on" : "off");
        if (ControlPanelTab.SETTINGS.open()) {
            InterfaceComponent button = Interfaces.newQuery()
                .containers(116)
                .types(InterfaceComponent.Type.CONTAINER)
                .grandchildren(false)
                .actions("Toggle Accept Aid")
                .results()
                .first();
            if (button != null && button.interact("Toggle Accept Aid")) {
                return Execution.delayUntil(() -> isAcceptingAid() == accept, 300, 600);
            }
        }
        return false;
    }

    /**
     * @deprecated This method has been replaced
     * Use {@link AllSettings#ALWAYS_HIDE_ROOFS} instead.
     */

    @Deprecated
    public static boolean areRoofsAlwaysHidden() {
        return OpenClientPreferences.isHidingRoofs();
    }

    /**
     * @deprecated This method has been replaced
     * Use {@link AllSettings#ALWAYS_HIDE_ROOFS} instead.
     */
    @Deprecated
    public static boolean toggleAlwaysHideRoofs() {
        return setAlwaysHideRoofs(!AllSettings.ALWAYS_HIDE_ROOFS.isEnabled());
    }

    /**
     * Toggles the option to always hide roofs.
     * Attempts to open the advanced menu options if they are not visible already
     *
     * @param enable the target state of the option
     * @return <code>true</code> if the option is already in the target state (<code>enable</code>), or if the option has been toggled successfully
     * @deprecated This method has been replaced
     * <p> Use {@link AllSettings#ALWAYS_HIDE_ROOFS} instead.
     */
    @Deprecated
    public static boolean setAlwaysHideRoofs(boolean enable) {
        boolean initial = AllSettings.ALWAYS_HIDE_ROOFS.isEnabled();
        return initial == enable || AllSettings.ALWAYS_HIDE_ROOFS.toggle();
    }

    /**
     * Opens the advanced options menu
     *
     * @return <code>true</code> if the menu is already open or the menu has been opened successfully
     */
    public static boolean openAdvancedOptionsMenu() {
        if (isAdvancedOptionsMenuOpen()) {
            return true;
        }
        log.info("Opening advanced options menu");
        if (ControlPanelTab.SETTINGS.open()) {
            final InterfaceComponent advancedOptionsButton = Interfaces.newQuery()
                .containers(116)
                .types(InterfaceComponent.Type.CONTAINER)
                .actions("All Settings")
                .results()
                .first();
            if (advancedOptionsButton != null) {
                return advancedOptionsButton.interact("All Settings")
                    && Execution.delayUntil(OptionsTab::isAdvancedOptionsMenuOpen, 700, 1000);
            }
        }
        return false;
    }

    /**
     * Closes the advanced options menu
     *
     * @return <code>true</code> if the menu is not open or the menu has been closed successfully
     */
    public static boolean closeAdvancedOptionsMenu() {
        if (!isAdvancedOptionsMenuOpen()) {
            return true;
        }
        log.info("Closing advanced options menu");
        final InterfaceComponent closeButton = Interfaces.newQuery()
            .containers(134)
            .types(InterfaceComponent.Type.SPRITE)
            .actions("Close")
            .results()
            .first();
        return closeButton != null && closeButton.interact("Close") && Execution.delayWhile(OptionsTab::isAdvancedOptionsMenuOpen, 100);
    }

    /**
     * @return <code>true</code> if the <code>InterfaceContainer</code> of the advanced options menu is loaded
     */
    public static boolean isAdvancedOptionsMenuOpen() {
        return !Interfaces.newQuery().containers(134).types(InterfaceComponent.Type.SPRITE).actions("Close").results().isEmpty();
    }

    /**
     * @return the currently opened <code>Tab</code>
     */
    @Nullable
    public static Menu getOpenedMenu() {
        return Arrays.stream(Menu.values()).filter(Menu::isOpen).findAny().orElse(null);
    }

    public static boolean isUsingSidePanels() {
        final Varbit varbit = Varbits.load(4607);
        return varbit != null && varbit.getValue() == 1;
    }

    public static boolean areSidePanelsClosableByHotkeys() {
        final Varbit varbit = Varbits.load(4611);
        return varbit != null && varbit.getValue() == 0;
    }

    public static boolean isAcceptAidEnabled() {
        final Varbit varbit = Varbits.load(4180);
        return varbit != null && varbit.getValue() == 1;
    }

    /**
     * @return <code>true</code> if the option to enable shift click dropping is enabled
     * @deprecated This method has been replaced
     * Use {@link AllSettings#SHIFT_DROPPING} instead.
     */
    @Deprecated
    public static boolean isShiftDroppingEnabled() {
        final Varbit varbit = Varbits.load(11556);
        return varbit != null && varbit.getValue() == 1;
    }

    /**
     * Toggles the option to enable shift click dropping
     *
     * @param enable the target state of the option
     * @return <code>true</code> if the option is already in the target state (<code>enable</code>),
     * or the option has been toggled successfully
     * @deprecated This method has been replaced
     * Use {@link AllSettings#SHIFT_DROPPING} instead.
     */
    @Deprecated
    public static boolean setShiftDropping(boolean enable) {
        if (AllSettings.SHIFT_DROPPING.isEnabled() == enable) {
            return true;
        } else if (openAdvancedOptionsMenu()) {
            return AllSettings.SHIFT_DROPPING.toggle();
        }
        return false;
    }

    /**
     * Changes the state of the shift click dropping option to the opposite of the current state
     *
     * @return <code>true</code> if the option has been toggled successfully
     * @deprecated This method has been replaced
     * Use {@link AllSettings#SHIFT_DROPPING} instead.
     */
    @Deprecated
    public static boolean toggleShiftDropping() {
        return setShiftDropping(!AllSettings.SHIFT_DROPPING.isEnabled());
    }

    public static int getDropWarningValue() {
        final var varbit = Varbits.load(5412);
        return varbit != null ? varbit.getValue() : 0;
    }

    /**
     * @deprecated This method has been replaced.
     * Use {@link AllSettings#INTERFACES_CLOSABLE_WITH_ESCAPE} instead.
     */
    @Deprecated
    public static boolean areInterfacesCloseableWithEsc() {
        Varbit varbit = Varbits.load(4681);
        return varbit != null && varbit.getValue() == 1;
    }

    /**
     * Changes the NPC 'Attack' Option that is selected in the opens menu
     *
     * @param attackOption The NPC 'Attack' Option you would like to be selected
     * @return <code>true</code> if the option is already in the target state (<code>enable</code>), or the option has been set successfully
     */
    public static boolean setNpcAttackOption(AttackOption attackOption) {
        if (attackOption == null) {
            return false;
        }
        log.info("Setting npc attack option {}", attackOption);
        AttackOption currentOption = getNpcAttackOption();
        if (currentOption != null && currentOption == attackOption) {
            return true;
        }
        if (OptionsTab.Menu.CONTROLS.isOpen() || OptionsTab.Menu.CONTROLS.open()) {
            InterfaceComponentQueryResults menuStateIndicatorArrows = Interfaces.newQuery()
                .containers(116)
                .grandchildren(true)
                .types(InterfaceComponent.Type.SPRITE)
                .sprites(297, 773, 788)
                .widths(16)
                .heights(16)
                .visible()
                .results();
            if (menuStateIndicatorArrows.size() != 2) {
                log.warn(
                    "Identified {} instead of 2 interfaces while looking for the menu state indicator arrows",
                    menuStateIndicatorArrows.size()
                );
                return false;
            }
            InterfaceComponent menuStateIndicatorArrow = menuStateIndicatorArrows.get(1);
            if (menuStateIndicatorArrow.getSpriteId() == 788) {
                if (menuStateIndicatorArrow.click()) {
                    Execution.delayUntil(() -> menuStateIndicatorArrow.getSpriteId() == 773, 300, 600);
                }
            }
            InterfaceComponent select = Interfaces.newQuery()
                .containers(116)
                .types(InterfaceComponent.Type.LABEL)
                .grandchildren(true)
                .actions("Select")
                .texts(attackOption.getInterfaceText())
                .results()
                .first();
            return select != null && select.interact("Select") && Execution.delayUntil(
                () -> getNpcAttackOption() == attackOption,
                400,
                800
            );
        }
        return false;
    }

    /**
     * Changes the Player 'Attack' Option that is selected in the options menu
     *
     * @param attackOption The Player 'Attack' Option you would like to be selected
     * @return <code>true</code> if the option is already in the target state (<code>enable</code>), or the option has been set successfully
     */
    public static boolean setPlayerAttackOption(AttackOption attackOption) {
        if (attackOption == null) {
            return false;
        }
        log.info("Setting player attack option {}", attackOption);
        AttackOption currentOption = getPlayerAttackOption();
        if (currentOption != null && currentOption == attackOption) {
            return true;
        }
        if (Menu.CONTROLS.isOpen() || Menu.CONTROLS.open()) {
            InterfaceComponentQueryResults menuStateIndicatorArrows = Interfaces.newQuery()
                .containers(116)
                .grandchildren(true)
                .types(InterfaceComponent.Type.SPRITE)
                .sprites(297, 773, 788)
                .widths(16)
                .heights(16)
                .visible()
                .results();
            if (menuStateIndicatorArrows.size() != 2) {
                log.warn(
                    "Identified {} instead of 2 interfaces while looking for the menu state indicator arrows",
                    menuStateIndicatorArrows.size()
                );
                return false;
            }
            InterfaceComponent menuStateIndicatorArrow = menuStateIndicatorArrows.get(0);
            if (menuStateIndicatorArrow.getSpriteId() == 788) {
                if (menuStateIndicatorArrow.click()) {
                    Execution.delayUntil(() -> menuStateIndicatorArrow.getSpriteId() == 773, 300, 600);
                }
            }
            InterfaceComponent select = Interfaces.newQuery()
                .containers(116)
                .types(InterfaceComponent.Type.LABEL)
                .grandchildren(true)
                .actions("Select")
                .texts(attackOption.getInterfaceText())
                .results()
                .first();
            return select != null && select.interact("Select") && Execution.delayUntil(
                () -> getPlayerAttackOption() == attackOption,
                400,
                800
            );
        }
        return false;
    }

    public static AttackOption getNpcAttackOption() {
        int npcAttackOptionSetting = Varps.getAt(1306).getValue();
        switch (npcAttackOptionSetting) {
            case 0:
                return AttackOption.DEPENDS_ON_COMBAT_LEVELS;
            case 1:
                return AttackOption.ALWAYS_RIGHT_CLICK;
            case 2:
                return AttackOption.LEFT_CLICK_WHERE_AVAILABLE;
            case 3:
                return AttackOption.HIDDEN;
            default:
                log.warn("Can not determine Npc 'Attack' Option setting for value {}", npcAttackOptionSetting);
        }
        return null;
    }

    public static AttackOption getPlayerAttackOption() {
        int playerAttackOptionSetting = Varps.getAt(1107).getValue();
        switch (playerAttackOptionSetting) {
            case 0:
                return AttackOption.DEPENDS_ON_COMBAT_LEVELS;
            case 1:
                return AttackOption.ALWAYS_RIGHT_CLICK;
            case 2:
                return AttackOption.LEFT_CLICK_WHERE_AVAILABLE;
            case 3:
                return AttackOption.HIDDEN;
            default:
                log.warn("Can not determine Player 'Attack' Option setting for value {}", playerAttackOptionSetting);
        }
        return null;
    }

    private static boolean isSearching() {
        if (isAdvancedOptionsMenuOpen()) {
            InterfaceComponent searchbar = getSearchBar();
            //If it's null it's because it's been clicked into and something is being searched for.
            return searchbar == null;
        }
        return false;

    }

    @Nullable
    private static InterfaceComponent getSearchBar() {
        return Interfaces.newQuery()
            .containers(134)
            .grandchildren(false)
            .types(InterfaceComponent.Type.CONTAINER)
            .actions("Search")
            .results()
            .first();
    }

    @Nullable
    private static String getCurrentSearchTerm() {
        if (!isSearching()) {
            return null;
        }
        final InterfaceComponent searchTerm = Interfaces.newQuery()
            .containers(134)
            .types(InterfaceComponent.Type.LABEL)
            .texts(SEARCH_PATTERN)
            .grandchildren(false)
            .results()
            .first();

        String text;
        if (searchTerm == null || (text = searchTerm.getText()) == null) {
            return null;
        } else {
            return text.substring(0, text.length() - 1);
        }
    }

    /**
     * The different tabs/menus inside the options menu
     */
    public enum Menu implements Openable {
        CONTROLS("Controls"),
        AUDIO("Audio"),
        DISPLAY("Display");
        private final String action;

        Menu(String action) {
            this.action = action;
        }

        /**
         * Opens the tab/category, and opens the options tab if it is not already open
         *
         * @return <code>true</code> if the tab is already open, or has been opened successfully
         */
        @Override
        public boolean open() {
            log.info("Opening {} settings", this);
            Menu menu = OptionsTab.getOpenedMenu();
            if (menu == null) {
                if (ControlPanelTab.SETTINGS.open()) {
                    menu = OptionsTab.getOpenedMenu();
                }
            }
            if (Objects.equals(this, menu)) {
                return true;
            }
            if (ControlPanelTab.SETTINGS.open()) {
                InterfaceComponent button = Interfaces.newQuery()
                    .containers(116)
                    .types(InterfaceComponent.Type.CONTAINER)
                    .grandchildren(false)
                    .actions(action)
                    .visible()
                    .results()
                    .first();
                if (button != null && button.interact(action)) {
                    return Execution.delayUntil(() -> equals(getOpenedMenu()), 1000);
                }
            }
            return false;
        }

        /**
         * @return <code>true</code> if the tab/category is selected, and the options tab is open or has been opened successfully
         */
        @Override
        public boolean isOpen() {
            if (!ControlPanelTab.SETTINGS.isOpen()) {
                return false;
            }
            return Interfaces.newQuery()
                .containers(116)
                .types(InterfaceComponent.Type.CONTAINER)
                .grandchildren(false)
                .actions(action)
                .visible()
                .results()
                .isEmpty();
        }

        private String getAction() {
            return action;
        }
    }

    /**
     * The different settings regarding NPC and player attack options
     */
    public enum AttackOption {
        DEPENDS_ON_COMBAT_LEVELS("Depends on combat levels"),
        ALWAYS_RIGHT_CLICK("Always right-click"),
        LEFT_CLICK_WHERE_AVAILABLE("Left-click where available"),
        HIDDEN("Hidden");
        private final String interfaceText;

        AttackOption(String interfaceText) {
            this.interfaceText = interfaceText;
        }


        private String getInterfaceText() {
            return interfaceText;
        }
    }

    public enum CanvasMode {
        FIXED,
        RESIZABLE
    }

    public enum AllSettings {
        ACCEPT_AID("accept", 4180, 1),
        DISABLE_TABLET_WARNINGS("disable tab", -1, -1), //Button that disables all tablet warnings, is not a checkbox
        DISABLE_TELEPORT_WARNINGS("disable tele", -1, -1), //Button that disables all teleport warnings, is not a checkbox
        ENABLE_TABLET_WARNINGS("enable tab", -1, -1),//Button that enables all tablet warnings, is not a checkbox
        ENABLE_TELEPORT_WARNINGS("enable tele", -1, -1),//Button that enables all teleport warnings, is not a checkbox
        INTERFACES_CLOSABLE_WITH_ESCAPE("esc", 4681, 1),
        ALWAYS_HIDE_ROOFS("hide r", -1, -1) {
            @Override
            public boolean isEnabled() {
                //not stored as varp/varbit
                return OpenClientPreferences.isHidingRoofs();
            }
        },
        MIDDLE_MOUSE_CAMERA("middle", 4134, 0),
        MOUSE_SCROLL_ZOOMS("scroll w", 6357, 0),
        SHIFT_DROPPING("shift", 11556, 1),
        TELEPORT_TO_TARGET_WARNING("to tar", 236, 1),
        DAREEYAK_TELEPORT_WARNING("yak ", 6284, 1),
        CARRALLANGAR_TELEPORT_WARNING("gar ", 6285, 1),
        ANNAKARL_TELEPORT_WARNING("arl ", 6286, 1),
        GHORROCK_TELEPORT_WARNING("rock ", 6287, 1),
        DAREEYAK_TABLET_WARNING("tablet dar", 3930, 0),
        CARRALLANGAR_TABLET_WARNING("tablet car", 2325, 0),
        ANNAKARL_TABLET_WARNING("tablet ann", 2324, 0),
        GHORROCK_TABLET_WARNING("tablet gho", 3931, 0),
        CEMETERY_TABLET_WARNING("cem", 2322, 0),
        WILDERNESS_CRABS_TABLET_WARNING("crab", 3932, 0),
        ICE_PLATEAU_TABLET_WARNING("ice", 2323, 0),
        DROP_ITEM_WARNING("drop", 5411, 1);

        private final String searchTerm;
        private final int varbit;
        private final int enabledValue;

        AllSettings(String searchTerm, int varbit, int enabledValue) {
            this.searchTerm = searchTerm;
            this.varbit = varbit;
            this.enabledValue = enabledValue;
        }

        public boolean isEnabled() {
            Varbit varbit = Varbits.load(this.varbit);
            return varbit != null && varbit.getValue() == enabledValue;
        }

        public boolean toggle() {
            log.info("Toggling {}", this);
            boolean initialState = isEnabled();
            if (OptionsTab.openAdvancedOptionsMenu()) {
                if (!isSearching()) {
                    final InterfaceComponent search = getSearchBar();
                    if (search != null && search.interact("Search")) {
                        Execution.delayUntil(OptionsTab::isSearching, 500, 1000);
                    }
                }
                if (isSearching()) {
                    String currentSearchTerm = getCurrentSearchTerm();
                    if (currentSearchTerm == null || currentSearchTerm.isEmpty()) {
                        Keyboard.type(searchTerm, false);
                        Execution.delayUntil(() -> searchTerm.equals(getCurrentSearchTerm()), 600, 1200);
                        currentSearchTerm = getCurrentSearchTerm();
                    }
                    if (currentSearchTerm != null && currentSearchTerm.equals(searchTerm)) {
                        final InterfaceComponent toggle = Interfaces.newQuery()
                            .containers(134)
                            .types(InterfaceComponent.Type.BOX)
                            .actions("Select", "Toggle")
                            .results()
                            .first();
                        if (toggle != null) {
                            return toggle.interact(Regex.getPatternContainingOneOf("Select", "Toggle"))
                                && Execution.delayUntil(() -> isEnabled() != initialState, 500, 1000);
                        }
                    } else {
                        Keyboard.typeKey(KeyEvent.VK_BACK_SPACE);
                    }
                }
            }
            return false;
        }
    }
}