package com.runemate.game.api.osrs.local.hud.interfaces;

import com.google.common.collect.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.framework.*;
import com.runemate.game.cache.io.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import lombok.extern.log4j.*;

@Log4j2
public final class OSRSWorldSelect {

    public static final InteractableRectangle WORLD_SELECTOR_OPEN_RECTANGLE = new InteractableRectangle(22, 463, 100, 35);
    private static final ConcurrentMap<AbstractBot, Collection<WorldOverview>> downloadedWorldOverviews =
        new MapMaker().initialCapacity(2).weakKeys().makeMap();


    public static boolean isOpen() {
        return OpenClient.isWorldSelectorOpen();
    }


    public static void close() {
        //TODO click instead
        OpenClient.setWorldSelectorOpen(false);
    }

    public static boolean open() {
        return !RuneScape.isLoggedIn() && WORLD_SELECTOR_OPEN_RECTANGLE.getInteractionPoint().click() && Execution.delayUntil(
            OSRSWorldSelect::isOpen, 1200, 2400);
    }

    public static boolean isSelected(final int world) {
        int current = getCurrent();
        return world == current || world + 300 == current;
    }


    public static boolean select(final int id) {
        log.info("Selecting world {}", id);
        Iterator<WorldOverview> iterator = getWorlds(worldOverview -> worldOverview.getId() == id).iterator();
        if (!iterator.hasNext()) {
            return false;
        }
        WorldOverview next = iterator.next();
        if (!(next instanceof OSRSWorldOverview)) {
            return false;
        }
        OSRSWorldOverview world = (OSRSWorldOverview) next;
        boolean isCurrentWorldMembersOnly = OpenClient.isMembersWorld();
        boolean isDesiredWorldMembersOnly = world.isMembersOnly();
        if (isCurrentWorldMembersOnly != isDesiredWorldMembersOnly) {
            OpenClient.setMembersWorld(world.isMembersOnly());
            if (OpenClient.displayMembersItemData() != isDesiredWorldMembersOnly) {
                //ItemDefinition.ITEM_DEFINITION_CACHE.clear();
                //ItemDefinition.GROUND_ITEM_MODEL_CACHE.clear();
                //ItemDefinition.ITEM_SPRITE_CACHE.clear();
                OpenClient.displayMembersItemData(isDesiredWorldMembersOnly);
            }
        }
        OpenClient.setCurrentWorldAddress(world.getAddress());
        OpenClient.setCurrentWorld(id);
        OpenClient.setCurrentWorldType(world.getMarkerBits());
        int primaryServerPort = 43594;
        int secondaryServerPort = 443;
        OpenClient.setPrimaryServerPort(primaryServerPort);
        OpenClient.setSecondaryServerPort(secondaryServerPort);
        OpenClient.setPrimaryServerPort(primaryServerPort);
        return true;
    }

    //
    public static WorldQueryResults getWorlds(final Predicate<WorldOverview> filter) {
        final ArrayList<WorldOverview> worlds = new ArrayList<>();
        AbstractBot bot = Environment.getBot();
        if (downloadedWorldOverviews.containsKey(bot)) {
            for (WorldOverview world : downloadedWorldOverviews.get(bot)) {
                if (filter == null || filter.test(world)) {
                    worlds.add(world);
                }
            }
        } else {
            try {
                byte[] response = OpenWorld.queryWorldList();
                Js5InputStream stream = new Js5InputStream(response);
                int length = stream.readInt();
                int size = stream.readUnsignedShort();
                for (int index = 0; index < size; index++) {
                    int id = stream.readUnsignedShort();
                    int type = stream.readInt();
                    String address = stream.readCStyleLatin1String();
                    String activity = stream.readCStyleLatin1String();
                    int location = stream.readUnsignedByte();
                    int population = stream.readUnsignedShort();
                    OSRSWorldOverview world = new OSRSWorldOverview(id, type, activity, address, location, population);
                    downloadedWorldOverviews.computeIfAbsent(bot, list -> new ArrayList<>()).add(world);
                    if (filter == null || filter.test(world)) {
                        worlds.add(world);
                    }
                }
            } catch (IOException e) {
                log.warn("Failed to download and cache the world list", e);
            }
        }
        return new WorldQueryResults(worlds, null);
    }

    public static InteractableRectangle getBounds(int index) {
        //When there's 18 rows
        //I've seen 18 rows when there were 88 worlds
        //I've also seen 18 rows and 6 columns instead of the usual 5 when there were 104 worlds...
        /*int column = index / 18;
        int row = index % 18;
        int x = 158 + (93 * column);
        int y = 52 + (24 * row);*/

        //When there's 19 rows (and 5 columns as a result?)
        //I've seen 19 rows when there were 91 & 94 worlds
        //int column = index / 19;
        //int row = index % 19;
        //int x = 158 + (93 * column);
        //int y = 37 + (24 * row);

        //When there's 20 rows (and 5 columns as a result?)
        //I've seen 20 rows when there were 96 & 99 worlds
        int column = index / 20;
        int row = index % 20;
        int x = 158 + (93 * column);
        int y = 22 + (24 * row);
        //width=88, height=19. We shrink it to remove edges.
        return new InteractableRectangle(x, y, 82, 12);
    }


    public static int getCurrent() {
        return OpenClient.getCurrentWorld();
    }


    public static boolean isCurrentWorldMembersOnly() {
        return OpenClient.isMembersWorld();
    }
}
