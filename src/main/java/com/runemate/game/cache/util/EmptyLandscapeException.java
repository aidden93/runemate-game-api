package com.runemate.game.cache.util;

public class EmptyLandscapeException extends RuntimeException {
    public EmptyLandscapeException() {
        super();
    }

    public EmptyLandscapeException(String message) {
        super(message);
    }
}
