package com.runemate.ui.control;

import com.runemate.game.internal.*;
import java.net.*;
import java.util.*;
import javafx.application.*;
import javafx.event.*;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.scene.effect.*;
import javafx.scene.layout.*;

public class ModalOverlay<T> extends VBox implements Initializable {

    private static final Effect BLUR_EFFECT = new GaussianBlur(5);

    protected final Node node;

    @FXML
    private VBox contentContainer;

    public ModalOverlay(Node node) {
        this.node = node;
        FXUtil.loadFxml(this, "/fxml/modal_overlay.fxml");
    }

    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {
        parentProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue instanceof Pane) {
                ((Pane) newValue).getChildren().forEach(n -> {
                    if (!Objects.equals(n, this)) {
                        n.setEffect(BLUR_EFFECT);
                    }
                });
            }
        });
        contentContainer.getChildren().setAll(node);
    }

    @FXML
    protected void onCloseButtonClicked(final ActionEvent event) {
        close(event, null);
    }

    /**
     * Closes the overlay and exits the nested event loop (unblocking {@link #getResult()})
     *
     * @see Platform#exitNestedEventLoop(Object, Object)
     * @see #getResult()
     */
    public void close(ActionEvent event, T result) {
        Platform.exitNestedEventLoop(node, result);
        if (getParent() instanceof Pane) {
            final Pane parent = (Pane) this.getParent();
            parent.getChildren().remove(this);
            parent.getChildren().forEach(n -> n.setEffect(null));
        }
    }

    /**
     * Enter a nested event loop and block until {@link #close(ActionEvent, Object)} call is made
     *
     * @see Platform#enterNestedEventLoop(Object)
     * @see #close(ActionEvent, Object)
     */
    @SuppressWarnings("unchecked")
    @InternalAPI
    public T getResult() {
        return (T) Platform.enterNestedEventLoop(node);
    }
}
